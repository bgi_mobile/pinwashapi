<?php
// 'user' object
class AddVehicle{

  // database connection and table name
  private $conn;
  private $table_name = "vehicle";

  // object properties
  public $id;
  public $phone;
  public $pwd;
  public $created;

  // constructor
  public function __construct($db){
    $this->conn = $db;
  }

  function addVehicle($data){
    $name=isset($data["name"]) ? $data["name"] : "";
    $vehicleNo=isset($data["vehicle_no"]) ? $data["vehicle_no"] : "";
    $createdBy=isset($data["created_by"]) ? $data["created_by"] : "";



    $query = "INSERT INTO
    " . $this->table_name . "
    SET
    name=:name, vehicle_no=:vehicle_no,created_by=:created_by";

    // prepare query statement
    $stmt = $this->conn->prepare($query);

    // bind new values
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':vehicle_no', $vehicleNo);
    $stmt->bindParam(':created_by', $createdBy);

    //$stmt->bindParam(':city', $city);

    // execute the query
    if($stmt->execute()){
      return true;
    }

    return false;
  }
}
