<?php

//dashboard object
class Service{

  // database connection and table name
  private $conn;
  private $table_name = "service";

  // object properties
  public $id;
  public $uid;
  public $pwd;
  public $created;

  // constructor
  public function __construct($db){
    $this->conn = $db;
  }

  // read orders
  function serviceList($userid){
      //echo $userid; die();
    // select all query
    $query = "SELECT s.user_id,
    s.service_name,s.company_name,s.type_service,s.description,s.email,s.phone_number,s.address,
    s.postal_code,s.amount,s.duration,s.created_datetime,s.updated_datetime,c.name as country,ci.name as city,st.name as state
    FROM
    " . $this->table_name . " s
    LEFT JOIN
    countries c
    ON s.country = c.id
    LEFT JOIN
    cities ci
    ON s.city = ci.id
    LEFT JOIN
    states st
    ON s.state = st.id
    where s.user_id = ? and s.status = ?
    ";



    // prepare query statement
    $stmt = $this->conn->prepare($query);
    $status = '1';
    // bind id of product to be updated
    $stmt->bindParam(1, $userid);
    $stmt->bindParam(2, $status);
    //print_r($stmt); die();
    // execute query
    $stmt->execute();

    return $stmt;
  }



}
