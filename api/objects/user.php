<?php
// 'user' object
class User{

    // database connection and table name
    private $conn;
    private $table_name = "user";

    // object properties
    public $id;
    public $phone;
    public $pwd;
    public $created;

    // constructor
    public function __construct($db){
        $this->conn = $db;
    }


    // check if given email exist in the database
function phonenoExists(){
    // query to check if email exists
    $query = "SELECT  id,phone,created_at,password
            FROM " . $this->table_name . "
            WHERE phone = ?
            LIMIT 0,1";

    // prepare the query
    $stmt = $this->conn->prepare( $query );

    // sanitize
    $this->phone=htmlspecialchars(strip_tags($this->phone));
    // bind given email value
    $stmt->bindParam(1, $this->phone);

    // execute the query
    $stmt->execute();

    // get number of rows
    $num = $stmt->rowCount();
    // if email exists, assign values to object properties for easy access and use for php sessions
    if($num>0){
        // get record details / values
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        // assign values to object properties
        $this->id = $row['id'];
        $this->phone = $row['phone'];
        $this->pwd = $row['password'];
        $this->created = $row['created_at'];
        // return true because email exists in the database
        return true;
    }

    // return false if email does not exist in the database
    return false;
}

function register($data){
  $name=isset($data["name"]) ? $data["name"] : "";
  $email=isset($data["email"]) ? $data["email"] : "";
  $phone=isset($data["phone"]) ? $data["phone"] : "";
  $vehicleNumber=isset($data["vehicle_number"]) ? $data["vehicle_number"] : "";
  $address1 = isset($data['address1']) ? $data['address1'] : "";
  $address2 = isset($data['address2']) ? $data['address2'] : "";
  $pinno = isset($data['pinno']) ? $data['pinno'] : "";
  $country = isset($data['country']) ? $data['country'] : "";
  $state = isset($data['state']) ? $data['state'] : "";
  $city = isset($data['city']) ? $data['city'] : "";
  $password = isset($data['password']) ? $data['password'] : "";
  $query = "INSERT INTO
          " . $this->table_name . "
          SET
          name=:name, email=:email,phone=:phone,vehicle_number=:vehicle_number,
          address1=:address1,address2=:address2,pinno=:pinno,state=:state,password=:password";

          // prepare query statement
          $stmt = $this->conn->prepare($query);

          // bind new values
          $stmt->bindParam(':name', $name);
          $stmt->bindParam(':email', $email);
          $stmt->bindParam(':phone', $phone);
          $stmt->bindParam(':vehicle_number', $vehicleNumber);
          $stmt->bindParam(':address1', $address1);
          $stmt->bindParam(':address2', $address2);
          $stmt->bindParam(':pinno', $pinno);
          //$stmt->bindParam(':country', $country);
          $stmt->bindParam(':state', $state);
          $stmt->bindParam(':password', $password);
          //$stmt->bindParam(':city', $city);

          // execute the query
          if($stmt->execute()){
            return true;
          }

          return false;
}

//update profile data
function updateProfileData($data,$userId){

  $phone =isset($data["phone"]) ? $data["phone"] : "";
  $email =isset($data["email"]) ? $data["email"] : "";
  $address1 =isset($data["address1"]) ? $data["address1"] : "";
  $address2 =isset($data["address2"]) ? $data["address2"] : "";
  $pinno = isset($data['pinno']) ? $data['pinno'] : "";
  $state = isset($data['state']) ? $data['state'] : "";

  // update query
  $query = "UPDATE
  " . $this->table_name . "
  SET

  phone=:phone,email=:email,address1=:address1,address2=:address2,pinno=:pinno,
  state=:state
  WHERE
  id = :id";

  // prepare query statement
  $stmt = $this->conn->prepare($query);
  // bind new values
  $stmt->bindParam(':phone', $phone);
  $stmt->bindParam(':email', $email);
  $stmt->bindParam(':address1', $address1);
  $stmt->bindParam(':address2', $address2);
  $stmt->bindParam(':pinno', $pinno);
  $stmt->bindParam(':state', $state);
  $stmt->bindParam(':id', $userId);
  // execute the query
  if($stmt->execute()){
    return true;
  }
  return false;
}
}
