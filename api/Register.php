<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/user.php';


// get database connection
$database = new Database();
$db = $database->getConnection();
// prepare dashboard object
$registerUser = new User($db);


// get keywords
$name=isset($_REQUEST["name"]) ? $_REQUEST["name"] : "";
$email=isset($_REQUEST["email"]) ? $_REQUEST["email"] : "";
$phone=isset($_REQUEST["phone"]) ? $_REQUEST["phone"] : "";
$vehicleNumber=isset($_REQUEST["vehicle_number"]) ? $_REQUEST["vehicle_number"] : "";
$address1 = isset($_REQUEST['address1']) ? $_REQUEST['address1'] : "";
$address2 = isset($_REQUEST['address2']) ? $_REQUEST['address2'] : "";
$pinno = isset($_REQUEST['pinno']) ? $_REQUEST['pinno'] : "";
$country = isset($_REQUEST['country']) ? $_REQUEST['country'] : "";
$state = isset($_REQUEST['state']) ? $_REQUEST['state'] : "";
$city = isset($_REQUEST['city']) ? $_REQUEST['city'] : "";
$password = isset($_REQUEST['password']) ? $_REQUEST['password'] : "";

        $password_enc = urlencode(base64_encode($password));


//echo $date.'df'.$time; die();
$data   =[];
if($name){
  $data  = [
    "name" => $name,
    "email" => $email,
    "phone" => $phone,
    "vehicle_number" => $vehicleNumber,
    "address1" => $address1,
    "address2" => $address2,
    "pinno" => $pinno,
    "state" => $state,
    "password" => $password_enc
  ];

    $insertData  = $registerUser->register($data);
    //print_r($insertData); die();
    if($insertData==1){
      // set response code
      http_response_code(200);
  $status= array('status' => "1","message" => "Registered successfully.");
      // response in json format
      echo json_encode(
        array(
          "Response"=> $status
        )
      );
    }

    }
    else{
      $status= array('status' => "0","message" => "Something Wrong...");
      // set response code - 404 Not found
      http_response_code(200);

      // tell the user no products found
      echo json_encode(
        array("Response"=> $status)
      );
    }
