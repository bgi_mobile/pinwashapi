<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rest-api-authentication-example/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/user.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate user object
$user = new User($db);

// get posted data
$data = $_POST;


// set product property values
$user->phone = $data['phone'];


//echo $user->password; exit;
//$userPassword = password_hash('ddd');
$password = $data['pwd'];

// $length = '8';
//
// $chars = "zyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA0123456789";
//
// $password = substr( str_shuffle( $chars ), 0, $length );
// $password_enc = urlencode(base64_encode($password));
$password = urlencode(base64_encode($password));
//echo $password; die();


 //$password =md5($password);
 //echo $password;

$username_exists = $user->phonenoExists();

// generate json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
// check if email exists and if password is correct
if($username_exists && $password==$user->pwd){

    $token = array(
       "iss" => $iss,
       "aud" => $aud,
       "iat" => $iat,
       "nbf" => $nbf,
       "data" => array(
           "id" => $user->id,
           "phone" => $user->phone,
           "pwd" => $user->pwd
       )
    );


    // set response code
    http_response_code(200);
    $data =[];
    // generate jwt
    $jwt = JWT::encode($token, $key);
    //
    $banner_arr=array();
$banner_arr["Response"]= array("message" => "Successful login.","jwt" => $jwt);


$banner_arr['Response']['status'] =1;
// set response code
http_response_code(200);

// tell the user login failed
echo json_encode($banner_arr);
    // $status= array('status' => "1","message" => "Successful login.","jwt" => $jwt);
    // echo json_encode(
    //         array(
    //             "Response"=> $status
    //         )
    //     );

}

// login failed
else{
  $banner_arr=array();
$banner_arr["Response"]= array("message" => "Login failed.");


$banner_arr['Response']['status'] =0;
// set response code
http_response_code(401);

// tell the user login failed
echo json_encode($banner_arr);

}
?>
