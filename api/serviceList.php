<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/serviceList.php';


// get database connection
$database = new Database();
$db = $database->getConnection();
// prepare dashboard object
$serviceList = new Service($db);

// get keywords
$jwt=isset($_REQUEST["jwt"]) ? $_REQUEST["jwt"] : "";
if($jwt){
  try{
    //decode jwt detailes
    $decoded = JWT::decode($jwt, $key, array('HS256'));
    $user->id = $decoded->data->id;
    $stmt = $serviceList->serviceList($user->id);
    $num = $stmt->rowCount();
    if($num>0){
      // orders array
      $service_arr=array();
      $service_arr["services"]=array();

      // retrieve our table contents
      // fetch() is faster than fetchAll()
      // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){ //print_r($row); die();
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
        //if(!empty($name)){
          $service_item=array(
            "service_name" => $service_name,
            "company_name" => $company_name,
            "type_service" => $type_service,
            "description" =>  htmlspecialchars(strip_tags($description)),
            "email" => $email,
            "phone_number" => $phone_number,
            "address" => htmlspecialchars(strip_tags($address)),
            "postal_code" => $postal_code,
            "amount" =>  $amount,
            "duration" => $duration,
            "country"  => $country,
            "city"  => $city,
            "state" => $state,
            "created_date" => date('Y-m-d', strtotime($created_datetime)),
            "updated_date" => date('Y-m-d', strtotime($updated_datetime)),
            "created_time" => date('H:i:s', strtotime($created_datetime)),
            "updated_time" => date('H:i:s', strtotime($updated_datetime))
          );
        //}
        array_push($service_arr["services"], $service_item);
      }


      $service_arr['Response']['status'] =1;

      // set response code - 200 OK
      http_response_code(200);

      // show products data in json format
      echo json_encode($service_arr);



    }
    else{
      $status= array('status' => "0","message" => "No services found.");
      // set response code - 404 Not found
      http_response_code(200);

      // tell the user no products found
      echo json_encode(
        array("Response"=> $status)
      );
    }
    //print_r($num); die();

  }
  // if decode fails, it means jwt is invalid
  catch (Exception $e){

    // set response code
    http_response_code(401);

    // show error message
    echo json_encode(array(
      "message" => "invalid data.",
      "error" => $e->getMessage()
    ));
  }
}
else{

  // set response code
  http_response_code(401);

  // tell the user access denied
  echo json_encode(array("message" => "Access denied."));
}
